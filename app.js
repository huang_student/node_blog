const handleBlogRouter = require('./src/router/blog.js')
const handleUserRouter = require('./src/router/user.js')
const { access } = require('./src/utils/log')
const querystring = require('querystring')
const { resolve } = require('path')
const { rejects } = require('assert')
const { get, set } = require('./src/db/redis')
//获取cookie过期时间
const getCookieExpires = () => {
   const d = new Date()
   //当前时间加上一天
   d.setTime(d.getTime() + (24 * 60 * 1000))
   return d.toGMTString()
}

//session数据
// const SESSION_DATA = {}

//用于处理post data
const getPostData = (req) => {
   const promise = new Promise((resolve, reject) => {
      if (req.method !== "POST") {
         resolve({})
         return
      }
      if (req.headers['content-type'] !== 'application/json') {
         resolve({})
         return
      }
      //接收数据
      let postData = ""
      req.on("data", chunk => {
         postData += chunk.toString()
      })
      req.on('end', chunk => {
         if (!postData) {
            resolve({})
            return
         }
         resolve(JSON.parse(postData))
      })
   })
   return promise
}

const serverHandle = (req, res) => {
   // 记录 access log
   access(`${req.method} -- ${req.url} -- ${req.headers['user-agent']} -- ${Date.now()}`)

   //设置返回格式
   res.setHeader('Content-type', 'application/json')

   const url = req.url
   req.path = url.split('?')[0]

   //获取query
   req.query = querystring.parse(url.split('?')[1])

   //解析cookie
   req.cookie = {}
   const cookieStr = req.headers.cookie || ''//k1=v1;k2=v2
   cookieStr.split(';').forEach(item => {
      if (!item) {
         return
      }
      const arr = item.split('=')
      const key = arr[0].trim()
      const val = arr[1].trim()
      req.cookie[key] = val
   })

   //解析 session
   // let needSetCookie = false //是否需要设置cookie中的userid
   // let userId = req.cookie.userid
   // if (userId) {//cookie存在该用户的userid （理解为客户端存放了userid）

   //    if (!SESSION_DATA[userId]) {//sessin没有存放username
   //       SESSION_DATA[userId] = {}//赋值为空对象
   //    }

   // } else {//cookie不存在该用户的userid （理解为客户端没有存放了userid）
   //    needSetCookie = true
   //    userId = `${Date.now()}_${Math.random()}`//设置成随机字符串
   //    SESSION_DATA[userId] = {}
   // }
   // req.session = SESSION_DATA[userId]

   //解析session
   let needSetCookie = false
   let  userId = req.cookie.userid
   if (!userId) {
      /*userId 并不存在，产生一个随机数存入到 redis 并且在返回信息到客户端时将该 userId 存入到 cookie
       这一步操作的目的在于说：
      用户知道未登录后，重新在当前页面登录时，会携带 cookie 发送给服务器端，
      此时，服务端可以通过 userId 获取到 redis 中的值（可能有值，也可能为空对象）
      */
      needSetCookie = true
      userId = `${Date.now()}_${Math.random()}`
      //初始化 redis 中 session 值
      set(userId,{})
   }
   //获取session
   req.sessionId = userId
   get(req.sessionId).then(sessionData => {
      if (sessionData == null) {
          //初始化 redis 中 session 值
         set(req.sessionId,{})
         //设置 session
         req.session = {}
      }else {
         req.session = sessionData
      }
      return getPostData(req)
   }).then(postData => {//解析post请求参数
      req.body = postData

      //处理 blog 路由 (对象是promise)
      const blogResult = handleBlogRouter(req, res)
      if (blogResult) {
         blogResult.then(blogData => {
            if (needSetCookie) {
               //操作cookie
               res.setHeader('Set-Cookie', `userid=${userId}; path=/; httpOnly; expires=${getCookieExpires()}`)
            }
            res.end(
               JSON.stringify(blogData)
            )
         })
         return
      }

      //处理 blog 路由 (对象时数据)
      // const blogData = handleBlogRouter(req,res)
      //    if (blogData) {
      //       res.end(
      //          JSON.stringify(blogData)
      //       )
      //       return
      //    }



      //处理user路由
      const userResult = handleUserRouter(req, res)
      if (userResult) {
         userResult.then(userData => {
            if (needSetCookie) {
               //操作cookie
               res.setHeader('Set-Cookie', `userid=${userId}; path=/; httpOnly; expires=${getCookieExpires()}`)
            }
            res.end(
               JSON.stringify(userData)
            )
         })
         return
      }

      // const userData = handleUserRouter(req, res)
      // if (userData) {
      //    res.end(
      //       JSON.stringify(userData)
      //    )
      //    return
      // }

      //未命中路由，返回404
      res.writeHead(404, { "Content-type": "text/plain" })
      res.write("404 Not Found\n")
      res.end()
   })



}
// env:process.env.NODE_ENV
module.exports = serverHandle
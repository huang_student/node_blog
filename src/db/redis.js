const {REDIS_CONF} = require('../conf/db')
const redis = require('redis')

//创建 redis 客户端
const redisClient = redis.createClient(REDIS_CONF.port,REDIS_CONF.host)

//错误处理
redisClient.on('error',err => {
    console.error(err)
})

//设置值
function set(key,val) {
    if (typeof val === 'object') {
        val=JSON.stringify(val)
    }
    redisClient.set(key, val, redis.print)
}

//获取值
function get(key) {//异步
    const promise = new Promise((resolve, reject) => {
        redisClient.get(key, (err,val) => {
            if (err) {
                reject(err)
                return
            }
            if (val == null) {
                resolve(null)
                return
            }
            try {
                resolve(
                    JSON.parse(val)//可能是对象被转换为字符串（存入时对象是已字符串的形式存入的）
                )  
            } catch (ex) {
                resolve(val)
            }
        })
    })
    return promise
}

module.exports = {
    set,
    get
}
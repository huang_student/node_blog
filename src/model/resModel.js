class BaseModel {
    /*
     data是返回到客户端的数据（对象类型），message是错误信息（字符串类型）
     如果errno=0，请求成功   通过data来获取数据
     如果errno=-1，请求失败 通过message来获取错误的具体原因
    */
    constructor(data, message) {
        //兼容（第一个参数（data）没有传递，只传递第二个参数）
        if (typeof data === 'string'){
            //将错误信息赋值给了message
            this.message = data
            data = null
            message = null
        }
        //赋值
        if (data) {
            this.data = data
        }
        if(message) {
            this.message = message
        }
    }
}

//请求成功
class SuccessModel extends BaseModel {
    constructor(data,message) {
        super(data, message)
        this.errno = 0
    }
}

//请求失败
class ErrorModel extends BaseModel {
    constructor(data, message) {
        super(data, message)
        this.errno = -1
    }
}

module.exports={
    SuccessModel,
    ErrorModel
}
const { getList, getDetail, newBlog, updateBlog, delBlog } = require('../controller/blog.js')
const { SuccessModel, ErrorModel } = require('../model/resModel.js')

//统一的登录验证函数
const logincheck = (req) => {
    if (!req.session.username) {
        return Promise.resolve(
            new ErrorModel('尚为登录')
        )
    }
}
const handleBlogRouter = (req, res) => {
    const id = req.query.id || ''//获取博客id 涉及俩个功能新增，更新博客
    const method = req.method
    //获取博客列表
    if (method === "GET" && req.path === "/api/blog/list") {
        //获取请求数据
        let author = req.query.author || ''
        const keyword = req.query.keyword || ''
        //去请求数据 (假数据)
        // const listData = getList(author,keyword)
        // return new SuccessModel(listData)
        
        if (req.query.isadmin) {
            //管理员界面
            const logincheckResult = logincheck(req)
            if (logincheckResult) {
                //未登录
                return logincheckResult
            }
            //强制查询自己的博客
            author = req.session.username
        }

        //去请求数据 （真数据）
        const result = getList(author, keyword)
        //此时返回是promise对象 而不是原先的直接返回的数据
        return result.then(listData => {
            return new SuccessModel(listData)
        })
    }

    //获取博客详情
    if (method === "GET" && req.path === "/api/blog/detail") {
        //去请求数据（假数据）
        // const detail = getDetail(id)
        // return new SuccessModel(detail)
    
        const result = getDetail(id)
        return result.then(detail => {
            return new SuccessModel(detail)
        })
    }

    //新增一篇博客
    if (method === "POST" && req.path === "/api/blog/new") {
        //获取post请求数据 (app.js中已经获取到了)
        // const postData = req.body
        // const id = newBlog(postData)
        // return new SuccessModel(id)
        const logincheckResult = logincheck(req)
        if (logincheckResult) {
            //未登录
            return logincheckResult
        }

        //req.body.author = 'zhangsan' //假数据，待开发登录时再改成真实数据
        req.body.author = req.session.username
        const postData = req.body
        const result = newBlog(postData)
        return result.then(id => {
            return new SuccessModel(id)
        })
    }

    //更新一篇博客
    if (method === "POST" && req.path === "/api/blog/update") {
        //获取post请求数据 (app.js中已经获取到了)
        // const postData = req.body
        // const result = updateBlog(id, postData)
        // if (result) {
        //     return new SuccessModel(result)
        // }
        // return new ErrorModel('更新博客失败')

        const logincheckResult = logincheck(req)
        if (logincheckResult) {
            //未登录
            return logincheckResult
        }

        const postData = req.body
        const result = updateBlog(id, postData)
        return result.then(val => {
            if (val) {
                return new SuccessModel(val)
            }else{
                return new ErrorModel('更新博客失败')
            }
            
        })
    }

    //删除一篇博客
    if (req.method === "POST" && req.path === "/api/blog/del") {
        // const result = delBlog(id)
        // if (result) {
        //     return new SuccessModel(result)
        // }
        // return new ErrorModel('删除博客失败')
        
        const logincheckResult = logincheck(req)
        if (logincheckResult) {
            //未登录
            return logincheckResult
        }

        //author = 'zhangsan' //假数据，待开发登录时再改成真实数据
        author = req.session.username
        const result =delBlog(id,author)
        return result.then(val => {
            if (val) {
                return new SuccessModel(val)
            } else {
                return new ErrorModel('删除博客失败')
            }
        })
    }
}
module.exports = handleBlogRouter
const login = require('../controller/user')
const { SuccessModel, ErrorModel } = require('../model/resModel')
const {set} = require('../db/redis')

//获取cookie过期时间
const getCookieExpires = () => {
   const d =new Date()
   //当前时间加上一天
   d.setTime(d.getTime() + (24 * 60 * 1000))
   return d.toGMTString()
}

const handleUserRouter = (req, res) => {
    const method = req.method
    //登录
    if (method === "POST" && req.path === "/api/user/login") {
        const postData = req.body
        const { username, password } = postData
        //    const username = req.query.username
        //    const password = req.query.password
        const result = login(username, password)
        return result.then(data => {
            if (data.username) {
                //操作cookie
                //res.setHeader('Set-Cookie', `username=${data.username}; path=/; httpOnly; expires=${getCookieExpires()}`)//path=/所有网页都生效
                req.session.username = data.username
                req.session.realname = data.realname
                //同步到 redis
                set(req.sessionId,req.session)
                return new SuccessModel()
            }
            return new ErrorModel('登录失败')
        })
    }
    //登录验证的测试
    // if (method === 'GET' && req.path === '/api/user/login-test') {
    //     if (req.session.username) {

    //         return Promise.resolve(new SuccessModel({session:req.session}))
    //     }
    //     return Promise.resolve(new ErrorModel('尚未登录'))
    // }
 }

module.exports = handleUserRouter
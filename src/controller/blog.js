const  { exec,escape } = require('../db/mysql')
const xss = require('xss')
const getList = (author,keyword) => {
    //先返回假数据（格式正确）
    // return [
    //     {
    //         id:1,
    //         title:'标题A',
    //         content:'内容A',
    //         createTime:1546610491112,
    //         author:'zhangsan'
    //     },
    //     {
    //         id:2,
    //         title:'标题B',
    //         content:'内容B',
    //         createTime:1546610491112,
    //         author:'lisi'
    //     },
    // ]
    /**
     * 因为 author 和 keyword 的值不确定，因此需要添加 1=1
     * （1）比如去掉 1=1时，如果 author 和 keyword 没有值，
     * 此时 sql 语句：select * from blog where order by createtime desc;报错 sql语句错误
     *  
     * （2）比如去掉 where 1=1时，author 和 keyword 都存在，
     * 此时sql语句：select * from blog and author ='${author}' and title like '%${keyword}%' 报错 sql语句错误
     **/ 
    
    
    let sql =`select * from blogs where 1=1 `
    if (author) {
        author = escape(author)
        sql += ` and author =${author} `
    }
    if (keyword) {
        keyword =escape(keyword)
        sql += ` and title like %${keyword}% `
    }
    sql += `order by createtime desc;`
   
    //返回 promise
    return exec(sql)
}

const getDetail = (id) => {
     //先返回假数据（格式正确）
    // return [
    //     {
    //         id:1,
    //         title:'标题A',
    //         content:'内容A',
    //         createTime:1546610491112,
    //         author:'zhangsan'
    //     }
    // ]
    id = escape(id)
     let sql = `select content,title,author,id from blogs where id=${id}`
     return exec(sql)
}

const newBlog = (blogData ={}) => {
    //blogData 是一个博客对象，包含 title content author 属性
    const title = xss(escape(blogData.title))
    const content = xss(escape(blogData.content))
    const author = escape(blogData.author)
    const createTime = Date.now()

    const sql = `insert into blogs (title, content, createtime, author)
                values (${title}, ${content}, ${createTime}, ${author})`

    return exec(sql).then(insertData => {
        //console.log(insertData)//打印出来看看返回什么信息
        return {
            id:insertData.insertId
        }
    })

}

const updateBlog = (id,blogData ={}) => {
    //id 就是要更新博客的 id
    //blogData 是一个博客对象，包含 title content 属性
    const content = xss(escape(blogData.content))
    const title = xss(escape(blogData.title))
    id = escape(id)
    let sql = `update blogs set title=${title}, content=${content} where id=${id}`
    return exec(sql).then(updateData => {
        // console.log(updateData)
        if (updateData.affectedRows > 0) {
           return true
        }
      return false
    })
} 

const delBlog = (id,author) => {
    //id 就是要删除博客的 id
    id = xss(escape(id))
    author = xss(escape(author))
    let sql = `delete from blogs where id=${id} and author=${author};`
    return exec(sql).then(delData => {
        console.log(delData)
        if (delData.affectedRows > 0) {
            return true            
        }
        return false
    })
} 
module.exports = {
    getList,
    getDetail,
    newBlog,
    updateBlog,
    delBlog
}